<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            $error=array('email'=>['Email y contraseña invalidos, vuelva a intentar ']);
            return response()->json(['error' => $error], 400);
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        //return response()->json([
            //'access_token' => $token,
            //'token_type' => 'bearer',
            //'expires_in' => auth()->factory()->getTTL() * 60
        //]);
        $ability=array([
            'action'=>'manage',
            'subject'=> 'all'
        ]);
        $extras=array(
            'eCommerceCartItemsCount'=>5,
        );
        $id='';
        $username='';
        $email='';
        $fullname='';
        $role='admin';
        $users=auth()->user();
        $data = json_decode($users);
  
        $id        = $data->id;
        $username  = $data->name;
        $email    = $data->email;
        $fullname = $data->name;
        $role = 'admin';
        
        $userData=array(
            'id'=>$id,
            'fullname'=> $fullname,
            'username'=>$username,
            'avatar'=> '/img/13-small.d796bffd.png',
            'email'=>$email,
            'role'=> $role,
            'ability'=> $ability,
            'extras'=> $extras

        );
        
        return response()->json([
            'userData' => $userData,
            'accessToken' => $token,
            'refreshToken' => $token
        ]);
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required',
            'password'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        } 
        $user=User::create(array_merge(
            $validator->validate(),
            ['password'=>bcrypt($request->password)]
        ));
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);

        //return response()->json([
        //    'message'=>'Usuario Registrado',
        //    'user'=> $user
        //],201);
    }
}